package Protoviz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.protege.editor.owl.model.OWLModelManager;
import org.protege.editor.owl.model.event.EventType;
import org.protege.editor.owl.model.event.OWLModelManagerListener;
import org.semanticweb.owlapi.model.*;
import javax.swing.border.LineBorder;

public class ProtovizView extends JPanel {

    private final OWLModelManager modelManager;
    private OWLController controller;
    // text component panel
    JPanel textComponentPanel = new JPanel(new BorderLayout());
    // sub class  panel
    JPanel subClassPanel = new JPanel(new GridLayout(5, 3, 4, 4));

    private final ActionListener refreshAction = e -> reset();

    // if active ontology is changed, reset() is called
    private final OWLModelManagerListener modelListener = event -> {
        if (event.getType() == EventType.ACTIVE_ONTOLOGY_CHANGED) {
            reset();
        }
    };

    public ProtovizView(OWLModelManager modelManager) {
        this.modelManager = modelManager;
        reset();
        modelManager.addListener(modelListener);
        add(textComponentPanel);
        add(subClassPanel);
    }

    //resets the view to have the root OWLModel as the focus
    private void reset() {
        OWLModel root = controller.getRoot();
        setFocus(root);
    }

    //disposes of the current view
    public void dispose() {
        
        modelManager.removeListener(modelListener);
    }

    /* 
     * Removes all elements from both panels in the GUI
     * and recreates the view with the selected OWLModel as the focus
     * displaying name of focused class, as well as buttons of all subclasses
     * and individuals
     */
    public void setFocus(OWLModel node) {
        textComponentPanel.removeAll();
        subClassPanel.removeAll();
        JLabel heading = new JLabel();
        heading.setText(node.getName());
        heading.setFont(new Font("Courier New", Font.BOLD, 100));
        textComponentPanel.add(heading, BorderLayout.CENTER);

        //create buttons for subclasses
        ArrayList<OWLModel> subs = node.getSubClasses(node);
        for (OWLModel sub : subs) {
            subClassPanel.add(makeSubclassButton(sub));
        }
        //create buttons for individuals
        Iterator<OWLNamedIndividual> nodesIterator = controller.getIndividuals(node);
        while (nodesIterator.hasNext()) {
            OWLNamedIndividual individual = nodesIterator.next();
            JButton individualButton = new JButton(individual.getIRI().getShortForm());
            formatIndividual(individualButton);
            subClassPanel.add(individualButton);
        }
        //Add a reset button
        textComponentPanel.add(makeResetButton(), BorderLayout.NORTH);

        //if node has a parent, show a back button to take user back to previous view
        if (node.hasParent()) {
            textComponentPanel.add(makeBackButton(node), BorderLayout.SOUTH);
        }

        revalidate();
        repaint();

    }

    //format an individual button to improve quality of appearance
    private JButton formatIndividual(JButton individualButton) {
        individualButton.setFont(new Font("Courier New", Font.BOLD, 15));
        individualButton.setBackground(Color.PINK);
        individualButton.setContentAreaFilled(true);
        individualButton.setBorder(new LineBorder(Color.BLACK));
        individualButton.setOpaque(true);
        return individualButton;
    }

    //create a button named 'Reset' which takes user back to view of the root node
    public JButton makeResetButton() {
        JButton resetButton = new JButton("Reset");
        ActionListener resetAction = e -> reset();
        resetButton.addActionListener(resetAction);
        return resetButton;
    }

    //create button for a subclass which, when clicked, calls setFocus with subclass as OWLModel
    public JButton makeSubclassButton(OWLModel sub) {
        ActionListener subFocusAction = e -> setFocus(sub);
        JButton subButton = new JButton(sub.getName());
        subButton.addActionListener(subFocusAction);
        subButton.setFont(new Font("Courier New", Font.BOLD, 15));
        subButton.setBackground(Color.YELLOW);
        subButton.setContentAreaFilled(true);
        subButton.setBorder(new LineBorder(Color.BLACK));
        subButton.setOpaque(true);
        return subButton;
    }

    //creates button to go back to previous view with previous OWLModel as the focus
    public JButton makeBackButton(OWLModel node) {
        JButton backButton = new JButton("Back");
        ActionListener backAction = e -> setFocus(node.getParent());
        backButton.addActionListener(backAction);
        return backButton;
    }

}
