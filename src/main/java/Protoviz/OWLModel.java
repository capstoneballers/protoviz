/*
 * Model class which holds information about the individual OWLClasses in OWLModel objects
 * and allows information about given classes to be stored and viewed such as their
 * attributes and relationships
 */
package Protoviz;

import java.util.ArrayList;
import java.util.Iterator;
import org.protege.editor.owl.model.hierarchy.OWLObjectHierarchyProvider;
import org.semanticweb.owlapi.model.OWLClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OWLModel {

    private static final Logger log = LoggerFactory.getLogger(ViewInitialiser.class);
    private final OWLClass owlClass;
    private String name;
    private final ArrayList<OWLModel> subclasses = new ArrayList<>();
    private final OWLModel parent;

    public OWLModel(String name, OWLClass owlClass, OWLModel parent) {
        this.owlClass = owlClass;
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return this.name;
    }

    public OWLClass getOWLClass() {
        return this.owlClass;
    }

    public void setName(String className) {
        this.name = className;
    }

    public void addSubClass(OWLModel subclass) {
        this.subclasses.add(subclass);
    }

    public ArrayList getSubClasses(OWLModel className) {
        return this.subclasses;
    }

    public OWLModel getParent() {
        return this.parent;
    }

    public boolean hasParent() {
        return this.parent != null;
    }

    @Override
    public String toString() {
        return this.getName();
    }

}
