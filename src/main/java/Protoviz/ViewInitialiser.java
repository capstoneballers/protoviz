/*
 * Class to intialise and display ProtovizView component
*/

package Protoviz;

import java.awt.BorderLayout;
import org.protege.editor.owl.ui.view.AbstractOWLViewComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ViewInitialiser extends AbstractOWLViewComponent {

    private static final Logger log = LoggerFactory.getLogger(ViewInitialiser.class);

    private ProtovizView protovizComponent;

    @Override
    protected void initialiseOWLView() throws Exception {
        setLayout(new BorderLayout());
        protovizComponent = new ProtovizView(getOWLModelManager());
        OWLController controller = new OWLController(getOWLModelManager(),protovizComponent);
        add(protovizComponent, BorderLayout.CENTER);
    }

    @Override
    protected void disposeOWLView() {
        protovizComponent.dispose();
    }
}
