/*
 * Class to interact with OWL objects in order to
 * manipulate and return data as required
 */
package Protoviz;

import java.util.Iterator;
import org.protege.editor.owl.model.OWLModelManager;
import org.protege.editor.owl.model.hierarchy.OWLObjectHierarchyProvider;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class OWLController {

    private final OWLModelManager modelManager;
    private final OWLOntology theOntology;
    private final ProtovizView protovizComponent;

    public OWLController(OWLModelManager modelManager, ProtovizView protovizComponent) {
        this.modelManager = modelManager;
        this.theOntology = modelManager.getActiveOntology();
        this.protovizComponent = protovizComponent;

    }

    //gets the root OWLModel of the ontology
    public OWLModel getRoot() {
        OWLObjectHierarchyProvider<OWLClass> hierarchy = modelManager.getOWLHierarchyManager().getOWLClassHierarchyProvider();
        //get root of ontology
        Iterator roots = hierarchy.getRoots().iterator();
        if (roots.hasNext()) {
            OWLClass rootClass = (OWLClass) roots.next();
            OWLModel root = setAllSubClasses(rootClass, hierarchy, null);
            return root;
        } 
        else {
            return null;
        }
    }

    /*
     * Appends all subclasses of a given OWLModel to it's arraylist 'subclasses'
     * and returns the given OWLModel with all subclasses attached
     */
    public static OWLModel setAllSubClasses(OWLClass root, OWLObjectHierarchyProvider<OWLClass> hierarchy, OWLModel parent) {
        String rootName = root.getIRI().getShortForm();
        OWLModel theRoot = new OWLModel(rootName, root, parent);
        Iterator children = hierarchy.getChildren(root).iterator();
        //set children of all subclasses recursively
        while (children.hasNext()) {
            OWLClass child = (OWLClass) children.next();
            theRoot.addSubClass(setAllSubClasses(child, hierarchy, theRoot));
        }
        return theRoot;
    }

    public OWLReasoner getReasoner() {
        OWLOntology theOntology = getOntology();
        return new Reasoner.ReasonerFactory().createReasoner(theOntology);
    }

    public OWLOntology getOntology() {
        return modelManager.getActiveOntology();
    }

    public String getClassName(OWLClass theClass) {
        return theClass.getIRI().getShortForm();
    }

    //return an iterator containing the individuals of the given class/node
    public Iterator<OWLNamedIndividual> getIndividuals(OWLModel node) {
        OWLClassExpression nodeClass = node.getOWLClass();
        OWLReasoner reasoner = getReasoner();
        NodeSet<OWLNamedIndividual> individualSet = reasoner.getInstances(nodeClass, true);
        Iterator<OWLNamedIndividual> nodesIterator = individualSet.getFlattened().iterator();
        return nodesIterator;
    }
}
